package ru.swayfarer.swl3.network.http;

import lombok.var;
import java.lang.reflect.Type;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
public class BodyDeserializingRule {

    public IFunction3<HttpHeaders, Type, String, Boolean> filterFun = (h, cl, str) -> true;
    public IFunction3<HttpHeaders, Type, String, Object> serializeFun = (h, cl, str) -> null;
    
    public Object convert(HttpHeaders headers, Type cl, String obj) 
    {
        if (filterFun.apply(headers, cl, obj))
        {
            return serializeFun.apply(headers, cl, obj);
        }
        
        return null;
    }
}
