package ru.swayfarer.swl3.network.http;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
public class BodySerializingRule {

    public IFunction2<HttpHeaders, Object, Boolean> filterFun = (h, o) -> true;
    public IFunction2<HttpHeaders, Object, String> serializeFun = (h, o) -> null;
    
    public String convert(HttpHeaders headers, Object obj) 
    {
        if (filterFun.apply(headers, obj))
        {
            return serializeFun.apply(headers, obj);
        }
        
        return null;
    }
}
