package ru.swayfarer.swl3.network.http;

import lombok.var;
import java.lang.reflect.Type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class DeseriazlizingRuleConfigurator {
    public HttpBodyConverterConfigurator configurator;
    public BodyDeserializingRule bodyDeserializingRule;
    
    public DeseriazlizingRuleConfigurator contentType(String... types)
    {
        var typesStream = ExtendedStream.of(types);
        return filter((h, cl, st) -> h.getValues("Content-Type").exStream().anyMatches((str) -> typesStream.anyMatches((s) -> s.equals(str.getStringValue()))));
    }
    
    public DeseriazlizingRuleConfigurator filter(@NonNull IFunction3<HttpHeaders, Type, String, Boolean> filter)
    {
        bodyDeserializingRule.filterFun = bodyDeserializingRule.filterFun.and(filter);
        return this;
    }
    
    public HttpBodyConverterConfigurator by(@NonNull IFunction3<HttpHeaders, Type, String, Object> fun)
    {
        bodyDeserializingRule.serializeFun = fun;
        return configurator;
    }
}
