package ru.swayfarer.swl3.network.http;

public enum EnumHttpMethod
{
    Get,
    Post,
    Put,
    Delete,
    Head,
    Connect,
    Options,
    Trace;
    
    public String getMethodName()
    {
        return name().toUpperCase();
    }
}
