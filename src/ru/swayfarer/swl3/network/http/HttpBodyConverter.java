package ru.swayfarer.swl3.network.http;

import lombok.var;
import java.lang.reflect.Type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("unchecked")
public class HttpBodyConverter {

    @NonNull
    public ExtendedList<IFunction3<HttpHeaders, Type, String, Object>> deserializeFuns = new ExtendedList<>();

    @NonNull
    public ExtendedList<IFunction2<HttpHeaders, Object, String>> serializeFuns = new ExtendedList<>();

    public String serialize(@NonNull HttpHeaders headers, @NonNull Object obj)
    {
        if (CollectionsSWL.isNullOrEmpty(serializeFuns))
            return obj.toString();
        
        for (var fun : serializeFuns)
        {
            var ret = fun.apply(headers, obj);
            
            if (ret != null)
                return ret;
        }
        
        return null;
    }

    public <T> T deserialize(@NonNull HttpHeaders headers, @NonNull Type classOfT, @NonNull String str)
    {
        if (CollectionsSWL.isNullOrEmpty(deserializeFuns))
        {
            if (classOfT == String.class || classOfT == Object.class)
                return (T) str;

            throw new UnsupportedOperationException("desealize fun was not set in " + this);
        }
        
        for (var fun : deserializeFuns)
        {
            var ret = fun.apply(headers, classOfT, str);
            
            if (ret != null)
                return (T) ret;
        }
        
        return null;
    }
    
    public HttpBodyConverter serializer(IFunction2<HttpHeaders, Object, String> fun)
    {
        this.serializeFuns.add(fun);
        return this;
    }
    
    public HttpBodyConverter deserializer(IFunction3<HttpHeaders, Type, String, Object> fun)
    {
        this.deserializeFuns.add(fun);
        return this;
    }
    
    public HttpBodyConverterConfigurator configure()
    {
        return new HttpBodyConverterConfigurator()
                .setConverter(this)
        ;
    }
}
