package ru.swayfarer.swl3.network.http;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpBodyConverterConfigurator {

    @NonNull
    public HttpBodyConverter converter;
    
    public DeseriazlizingRuleConfigurator deserialize()
    {
        var bodySerializingRule = new BodyDeserializingRule();
        converter.deserializeFuns.add(bodySerializingRule::convert);
        
        return new DeseriazlizingRuleConfigurator()
                .setConfigurator(this)
                .setBodyDeserializingRule(bodySerializingRule)
        ;
    }
    
    public SeriazlizingRuleConfigurator serialize()
    {
        var bodySerializingRule = new BodySerializingRule();
        converter.serializeFuns.add(bodySerializingRule::convert);
        
        return new SeriazlizingRuleConfigurator()
                .setConfigurator(this)
                .setBodySerializingRule(bodySerializingRule)
        ;
    }
}
