package ru.swayfarer.swl3.network.http;

import lombok.var;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.network.http.connection.HttpConnectionHelper;
import ru.swayfarer.swl3.network.http.connection.HttpSocketSource;
import ru.swayfarer.swl3.network.http.reader.HttpResponseReader;
import ru.swayfarer.swl3.network.http.utils.UrlInfoParser;
import ru.swayfarer.swl3.network.http.writer.HttpMessageWriter;
import ru.swayfarer.swl3.string.converters.StringConverters;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpSettings {

    public AtomicLong nextByteTimeout = new AtomicLong(2000);
    public AtomicBoolean useRedirects = new AtomicBoolean(true);
    
    @NonNull
    public String lineSplitter = "\r\n";
        
    @NonNull
    public HttpConnectionHelper connectionHelper = new HttpConnectionHelper();
    
    @NonNull
    public StringConverters headerConverters = new StringConverters().defaultTypes();
    
    @NonNull
    public HttpBodyConverter bodyConverter = new HttpBodyConverter();
    
    @NonNull
    public UrlInfoParser urlInfoParser = new UrlInfoParser();
    
    @NonNull
    public HttpSocketSource socketSource = new HttpSocketSource();
    
    @NonNull
    public HttpMessageWriter httpWriter = new HttpMessageWriter()
        .setLineSplitter(() -> lineSplitter)
    ;
    
    @NonNull
    public HttpResponseReader httpReader = new HttpResponseReader()
        .setLineSplitter(() -> lineSplitter)
        .setHttpSettings(() -> this)
    ;
}
