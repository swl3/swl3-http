package ru.swayfarer.swl3.network.http;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class SeriazlizingRuleConfigurator {
    public HttpBodyConverterConfigurator configurator;
    public BodySerializingRule bodySerializingRule;
    
    public SeriazlizingRuleConfigurator contentType(String... types)
    {
        var typesStream = ExtendedStream.of(types);
        return filter((h, o) -> h.getValues("Content-Type").exStream().anyMatches((str) -> typesStream.anyMatches((s) -> s.equals(str.getStringValue()))));
    }
    
    public SeriazlizingRuleConfigurator filter(@NonNull IFunction2<HttpHeaders, Object, Boolean> filter)
    {
        bodySerializingRule.filterFun = bodySerializingRule.filterFun.and(filter);
        return this;
    }
    
    public HttpBodyConverterConfigurator by(@NonNull IFunction2<HttpHeaders, Object, String> fun)
    {
        bodySerializingRule.serializeFun = fun;
        return configurator;
    }
}
