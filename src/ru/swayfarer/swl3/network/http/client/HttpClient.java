package ru.swayfarer.swl3.network.http.client;

import lombok.var;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.network.http.EnumHttpMethod;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;
import ru.swayfarer.swl3.network.http.request.HttpRequestContent;
import ru.swayfarer.swl3.network.http.response.HttpResponse;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpClient {

    @NonNull
    public HttpSettings settings = new HttpSettings();

    public HttpResponse post(@NonNull String address, @NonNull HttpRequestContent content)
    {
        return send(EnumHttpMethod.Post, address, content);
    }
    
    public HttpResponse get(@NonNull String address, @NonNull HttpRequestContent content)
    {
        return send(EnumHttpMethod.Get, address, content);
    }
    
    public HttpResponse send(@NonNull EnumHttpMethod method, @NonNull String address, @NonNull HttpRequestContent content)
    {
        var urlInfoParser = settings.getUrlInfoParser();
        var urlInfo = urlInfoParser.parse(address);
        var redirects = new ArrayList<String>();
        HttpResponse response = null;
        
        while (true)
        {
            var requests = new HttpClientRequests()
                .setHttpClient(this)
                .setMethod(method)
                .setUrlInfo(urlInfo)
                .setSocket(settings.getSocketSource().create(urlInfo));
            ;
            
            response = requests.request(content);

            boolean isBreak = true;
            
            int responseCode = response.getStatus().getCode();
            if (responseCode == 302) 
            {
                var property = response.getHeaders().getValue("Location");

                if (property != null) {
                    String newLocation = property.getStringValue();

                    if (!StringUtils.isBlank(newLocation)) 
                    {
                        if (redirects.contains(newLocation))
                        {
                            var newUri = urlInfoParser.parse(newLocation + urlInfo.getUriMapping());
                            
                            newUri.setLogin(urlInfo.getLogin());
                            newUri.setPassword(urlInfo.getPassword());
                            
                            urlInfo = newUri;
                            
                            redirects.add(newLocation);
                            
                            isBreak = false;
                        }
                    }
                }
            }
            
            if (isBreak)
                break;
        }

        return response;
    }
    
    public HttpHeaders newHeaders()
    {
        return new HttpHeaders()
                .setHttpSettings(this::getSettings)
        ;
    }
    
    public HttpRequestContent newRequestContent()
    {
        return new HttpRequestContent().setHttpSettings(() -> settings);
    }
}
