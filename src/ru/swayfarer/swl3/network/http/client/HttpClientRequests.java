package ru.swayfarer.swl3.network.http.client;

import lombok.var;
import java.net.Socket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.network.http.EnumHttpMethod;
import ru.swayfarer.swl3.network.http.request.HttpRequestContent;
import ru.swayfarer.swl3.network.http.request.HttpRequestTarget;
import ru.swayfarer.swl3.network.http.response.HttpResponse;
import ru.swayfarer.swl3.network.http.utils.NetworkUtils.HttpUrlInfo;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpClientRequests {
    
    @NonNull
    public HttpClient httpClient;
    
    @NonNull
    public EnumHttpMethod method = EnumHttpMethod.Get;
    
    @NonNull
    public HttpUrlInfo urlInfo;
    
    @NonNull
    public Socket socket;
    
    @SneakyThrows
    public HttpResponse request(HttpRequestContent content)
    {
        var headers = content.getHeaders();
        var standart = headers.standart();
        
        if (standart.getHost() == null)
            standart.host(urlInfo.getHost());
        
        if (!StringUtils.isEmpty(urlInfo.getLogin()) && !StringUtils.isEmpty(urlInfo.getPassword()))
        {
            if (standart.getAuthorization() == null)
                standart.basicAuth(urlInfo.getLogin(), urlInfo.getPassword());
        }
        
        var outStream = DataOutStream.of(socket.getOutputStream());
        
        var settings = httpClient.getSettings();
        var resuestStr = settings.getHttpWriter().write(new HttpRequestTarget(urlInfo.getUriMapping(), method), content);
        
        System.out.println("-------------------------------------");
        System.out.println(resuestStr);
        System.out.println("-------------------------------------");
        
        outStream.writeString(resuestStr);
        outStream.flush();
        
        var stream = socket.getInputStream();
        var dataStream = DataInStream.of(stream);
        
        HttpResponse response = settings.getHttpReader().read(dataStream, true);
        return response;
    }
}
