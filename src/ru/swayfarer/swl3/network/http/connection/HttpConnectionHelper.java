package ru.swayfarer.swl3.network.http.connection;

import lombok.var;
import java.net.HttpURLConnection;
import java.net.URL;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.network.http.parts.HttpBody;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;
import ru.swayfarer.swl3.network.http.parts.HttpStatus;
import ru.swayfarer.swl3.network.http.request.HttpRequestContent;
import ru.swayfarer.swl3.network.http.request.HttpRequestTarget;
import ru.swayfarer.swl3.network.http.response.HttpResponse;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
public class HttpConnectionHelper {

    public ReflectionsUtils reflectionsUtils = new ReflectionsUtils();
    
    public IFunction0<HttpSettings> httpSettings = () -> new HttpSettings();
    
    @SneakyThrows
    public HttpResponse newResponse(@NonNull HttpURLConnection connection)
    {
        var response = new HttpResponse();
        
        var is = connection.getInputStream();
        response.setStatus(new HttpStatus(connection.getResponseCode()));
        response.setMessage(connection.getResponseMessage());
        
        response.setBody(new HttpBody()
                .setHttpSettings(httpSettings)
                .setLazy(true)
                .setInStream(DataInStream.of(is)))
        ;
        
        var headers = new HttpHeaders();
        
        for (var header : connection.getHeaderFields().entrySet())
        {
            headers.getOrCreateValues(header.getKey()).addAll(header.getValue());
        }
        
        return response;
    }
    
    @SneakyThrows
    public HttpURLConnection newConnection(@NonNull String host, @NonNull HttpRequestContent content, @NonNull HttpRequestTarget target)
    {
        var mapping = target.getUriMapping();
        
        if (!host.endsWith("/") && !mapping.startsWith("/"))
            mapping = "/" + mapping;
        
        String urlStr = "http://" + host + mapping;
        
        URL url = new URL(urlStr);
        var connection = (HttpURLConnection) url.openConnection();
        
        String methodName = target.getMethod().getMethodName();
        connection.setRequestMethod(methodName);
       
        for (var header : content.getHeaders().getValues().entrySet()) {
            connection.setRequestProperty(header.getKey(), StringUtils.concatWith(";", header.getValue().toArray()));
        }

        var fieldsFilters = reflectionsUtils.fields().filters();

        var field = reflectionsUtils.fields().stream(connection)
            .filter(fieldsFilters.name().equal("doOutput"))
            .filter(fieldsFilters.type().equal(boolean.class))
            .findAny().orNull()
        ;
        
        field.set(connection, true);
        
        var bodyOut = connection.getOutputStream();
        System.out.println(connection.getRequestMethod());
        StreamsUtils.copyStream(content.getBody().in(), bodyOut, true, false);
        
        return connection;
    }
}
