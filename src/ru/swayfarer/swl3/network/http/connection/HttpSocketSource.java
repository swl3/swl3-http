package ru.swayfarer.swl3.network.http.connection;

import lombok.var;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLSocketFactory;

import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.network.http.utils.NetworkUtils.HttpUrlInfo;

@Data
@Accessors(chain = true)
public class HttpSocketSource {

    public Map<String, IFunction1<HttpUrlInfo, Socket>> protocolToSocket = new HashMap<>();
    
    public HttpSocketSource()
    {
        protocolToSocket.put("http", (info) -> createSimpleSocket(info.getHost(), info.getPort()));
        protocolToSocket.put("https", (info) -> createSslSocket(info.getHost(), info.getPort()));
    }
    
    @SneakyThrows
    protected Socket createSimpleSocket(String host, int port)
    {
        return new Socket(host, port);
    }
    
    @SneakyThrows
    protected Socket createSslSocket(String host, int port)
    {
        return SSLSocketFactory.getDefault().createSocket(host, port);
    }
    
    public Socket create(HttpUrlInfo info)
    {
        return protocolToSocket.get(info.getProtocol()).apply(info);
    }
}
