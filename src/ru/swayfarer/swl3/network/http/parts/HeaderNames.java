package ru.swayfarer.swl3.network.http.parts;

public interface HeaderNames {

    public String contentLength = "Content-Length";
    public String contentType = "Content-Type";
    public String userAgent = "User-Agent";
    public String authorization = "Authorization";
    public String host = "Host";
    public String connection = "Connection";
    
    
}
