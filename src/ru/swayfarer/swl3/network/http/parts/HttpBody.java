package ru.swayfarer.swl3.network.http.parts;

import lombok.var;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.reader.AdvancedStreamReader;
import ru.swayfarer.swl3.markers.OneShoot;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.StringValue;

@SuppressWarnings("unchecked")
@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpBody {

    public boolean lazy = true;
    
    @NonNull
    public IFunction0<HttpSettings> httpSettings = () -> new HttpSettings();
    
    @NonNull
    public IFunction0<HttpHeaders> headers = () -> null;
    
    @NonNull
    public String contentStr = "";
    
    public DataInStream inStream;

    public HttpBody setContent(@NonNull Object obj)
    {
        var str = httpSettings.apply().getBodyConverter().serialize(headers.apply(), obj);
        this.contentStr = StringUtils.isEmpty(str) ? "" : str;
        inStream = null;
        lazy = false;
        return this;
    }
    
    public <T> T getContent(@NonNull Class<T> classOfT)
    {
        return getContent((Type) classOfT);
    }
    
    public <T> T getContent(@NonNull Type classOfT)
    {
        if (classOfT == Object.class || classOfT == String.class)
        {
            return (T) getContentStr();
        }
        
        return httpSettings.apply().getBodyConverter().deserialize(headers.apply(), classOfT, getContentStr());
    }
    
    public String getContentStr()
    {
        if (lazy && inStream != null)
        {
            var reader = new AdvancedStreamReader();
            configureReaderLimit(reader);
            var bytes = reader.read(inStream);
            inStream.close();
            
            if (bytes != null)
                contentStr = new String(bytes, StandardCharsets.UTF_8);
            
            return contentStr;
        }
        
        return contentStr;
    }
    
    protected void configureReaderLimit(AdvancedStreamReader reader)
    {
        var headers = this.headers.apply();

        if (headers != null)
        {
            StringValue property = headers.getValue("Content-Length");
            
            if (property != null)
            {
                Long contentLenght = property.getValue(Long.class);
                
                if (contentLenght != null && contentLenght.longValue() > 0)
                {
                    reader.setMaxReadBytes(new AtomicLong(contentLenght));
                    return;
                }
            }
        }

        long timeout = getHttpSettings().apply().getNextByteTimeout().get();
        System.out.println("Max bytes time: " + timeout);
        reader.setMaxByteTime(timeout, TimeUnit.MILLISECONDS);
    }
    
    @OneShoot
    public DataInStream in()
    {
        if (lazy && inStream != null)
        {
            lazy = false;
            return inStream;
        }
        
        inStream = null;
        
        return BytesInStream.of(contentStr.getBytes());
    }
}
