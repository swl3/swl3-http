package ru.swayfarer.swl3.network.http.parts;

import lombok.var;
import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.string.StringValue;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpHeaders {

    @NonNull
    public IFunction0<HttpSettings> httpSettings = () -> new HttpSettings();
    
    @NonNull
    public Map<String, ExtendedList<String>> values = new HashMap<>();
    
    public StandartHeaders standart()
    {
        return new StandartHeaders(this);
    }
    
    public HttpHeaders value(@NonNull String name, @NonNull Object value)
    {
        getOrCreateValues(name).add(String.valueOf(value));
        return this;
    }
    
    public ExtendedList<String> getRawValues(@NonNull String name) 
    {
        var ret = values.get(name);
        return ret == null ? new ExtendedList<>() : ret;
    }
    
    public ExtendedList<String> getOrCreateValues(@NonNull String name) 
    {
        var ret = values.get(name);
        
        if (ret == null)
        {
            ret = new ExtendedList<String>();
            values.put(name, ret);
        }
        
        return ret;
    }
    
    public ExtendedList<StringValue> getValues(String name)
    {
       return getRawValues(name).map(this::createProperty);
    }
    
    protected StringValue createProperty(String propertyStr)
    {
        if (propertyStr == null)
            return null;
        
        return new StringValue(httpSettings.apply().getHeaderConverters(), () -> propertyStr)
                .updateValue()
        ;
    }
    
    public StringValue getValue(String headerName)
    {
        return getValues(headerName).first();
    }
    
    public StringValue getValue(String headerName, int index)
    {
        return getValues(headerName).get(index);
    }
}
