package ru.swayfarer.swl3.network.http.parts;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpStatus {

    public int code;
    
}
