package ru.swayfarer.swl3.network.http.parts;

import lombok.var;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.string.StringValue;

@Data
@Accessors(chain = true)
public class StandartHeaders implements HeaderNames {

    @NonNull
    public HttpHeaders wrappedHeaders;
    
    public Long getContentLenght()
    {
        return Double.valueOf(wrappedHeaders.getValue(contentLength).getStringValue()).longValue();
    }
    
    public StandartHeaders contentLenght(long lenght)
    {
        var values = wrappedHeaders.getOrCreateValues(contentLength);
        values.setAll(String.valueOf(lenght));
        
        return this;
    }
    
    public StringValue getContentType()
    {
        return wrappedHeaders.getValue(contentType);
    }
    
    public StandartHeaders contentType(@NonNull String type)
    {
        var values = wrappedHeaders.getOrCreateValues(contentType);
        values.setAll(type);
        
        return this;
    }
    
    public StandartHeaders authorization(String auth)
    {
        var values = wrappedHeaders.getOrCreateValues(authorization);
        values.setAll(auth);
        
        return this;
    }
    
    public StringValue getAuthorization()
    {
        return wrappedHeaders.getValue(authorization);
    }
    
    public StringValue getUserAgent()
    {
        return wrappedHeaders.getValue(userAgent);
    }

    public StandartHeaders connection(@NonNull String connect)
    {
        var values = wrappedHeaders.getOrCreateValues(connection);
        values.setAll(connect);
        
        return this;
    }
    
    public StringValue getConnection()
    {
        return wrappedHeaders.getValue(connection);
    }
    
    public StandartHeaders host(@NonNull String hostStr)
    {
        var values = wrappedHeaders.getOrCreateValues(host);
        values.setAll(hostStr);
        
        return this;
    }
    
    public StringValue getHost()
    {
        return wrappedHeaders.getValue(host);
    }
    
    public StandartHeaders userAgent(@NonNull String agent)
    {
        var values = wrappedHeaders.getOrCreateValues(userAgent);
        values.setAll(agent);
        
        return this;
    }
    
    public StandartHeaders basicAuth(@NonNull String user, @NonNull String password)
    {
        return authorization("Basic " + Base64.getEncoder().encodeToString((user + ":" + password).getBytes(StandardCharsets.UTF_8)));
    }
}
