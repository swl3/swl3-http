package ru.swayfarer.swl3.network.http.proxy;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.network.http.client.HttpClient;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ClientHelper {

    public IProxyProvider proxyProvider;
    public ClientSettings clientSettings = new ClientSettings();
    public HttpClient httpClient = new HttpClient();
    public ReflectionsUtils reflectionsUtils = new ReflectionsUtils();
    
    public <T, Ret extends T> Ret createClient(Class<T> clientInterface)
    {
        if (proxyProvider == null)
        {
            ExceptionsUtils.ThrowToCaller(
                    IllegalStateException.class, 
                    "Can't create client helper without proxy provider! Please, set proxy provider to", this
            );
        }
        
        return proxyProvider.createProxy(
                clientInterface, 
                createMethodListener()
                    .setSettings(this::getClientSettings)
                    .load(clientInterface))
        ;
    }

    public HttpClientInvocationHandler createMethodListener()
    {
        return new HttpClientInvocationHandler()
                .setHttpClient(this::getHttpClient)
                .setReflectionsUtils(this::getReflectionsUtils)
        ;
    }
    
}
