package ru.swayfarer.swl3.network.http.proxy;

public enum EnumParameterType
{
    Body,
    Address,
    Header,
    Simple
}
