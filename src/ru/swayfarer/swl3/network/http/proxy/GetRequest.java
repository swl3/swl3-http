package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.swl3.network.http.EnumHttpMethod;
import ru.swayfarer.swl3.reflection.annotations.AnnotationAlias;

@RequestMethod(method = EnumHttpMethod.Get)
@Retention(RUNTIME)
public @interface GetRequest {
    
    @AnnotationAlias(RequestMethod.class)
    public String[] headers() default {};
    
    @AnnotationAlias(RequestMethod.class)
    public String value() default "/";
}
