package ru.swayfarer.swl3.network.http.proxy;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.EnumHttpMethod;
import ru.swayfarer.swl3.network.http.client.HttpClient;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class HttpClientInvocationHandler implements IProxyProvider.IMethodListener{

    public IFunction0<ReflectionsUtils> reflectionsUtils = () -> new ReflectionsUtils();
    public Map<Method, HttpMethodListener> listeners = new ConcurrentHashMap<>();
    public IFunction0<HttpClient> httpClient;
    public String host;
    public IFunction0<ClientSettings> settings = () -> new ClientSettings();
    
    public HttpClientInvocationHandler load(Class<?> classOfClient)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        host = reflectionsUtils.annotations().findProperty()
                .type(String.class)
                .marker(HttpInterface.class)
                .name("host")
                .in(classOfClient)
                .get()
        ;

        var methodFilters = reflectionsUtils.methods().filters();

        reflectionsUtils.methods().stream(classOfClient)
            .filter(methodFilters.annotation().assignable(RequestMethod.class))
            .each((method) -> {
                var listener = createListener(method);
                listeners.put(method, listener);
            })
        ;
        
        return this;
    }
    
    public HttpClient getHttpClient()
    {
        return httpClient.apply();
    }
    
    public HttpMethodListener createListener(Method method)
    {
        var reflectionsUtils = getReflectionsUtils();
        var httpClient = getHttpClient();
        
        EnumHttpMethod httpMethod = reflectionsUtils.annotations().findProperty()
                .type(EnumHttpMethod.class)
                .marker(RequestMethod.class)
                .name("method")
                .in(method)
                .get()
        ;
        
        String uri = reflectionsUtils.annotations().findProperty()
                .type(String.class)
                .marker(RequestMethod.class)
                .name("value")
                .in(method)
                .get()
        ;

        String[] headersStrs = reflectionsUtils.annotations().findProperty()
                .type(String[].class)
                .marker(RequestMethod.class)
                .name("headers")
                .in(method)
                .get();
        
        ExtendedList<String[]> headers = new ExtendedList<>();
        
        for (var headerStr : headersStrs) 
        {
            var httpReader = httpClient.getSettings().getHttpReader();
            var header = httpReader.readHeader(headerStr);
            
            if (header != null)
            {
                headers.add(header);
            }
        }
        
        var params = new ParametersInfo();
        
        int nextIndex = 0;
        
        for (var param : method.getParameters()) 
        {
            var paramInfo = new ParameterInfo();
            paramInfo
                .setIndex(nextIndex)
                .setName(param.getName())
            ;
            
            var requestBodyAnnotation = reflectionsUtils.annotations().findAnnotation()
                    .in(param)
                    .ofType(RequestBody.class)
                    .get()
            ;
            
            if (requestBodyAnnotation != null)
            {
                paramInfo.setType(EnumParameterType.Body);
                params.setBodyIndex(nextIndex);
            }
            
            params.parameters.add(paramInfo);
            
            nextIndex ++;
        }
        
        var listener = new HttpMethodListener()
                .setBasicMethod(httpMethod)
                .setHost(host)
                .setHttpClient(httpClient)
                .setUri(uri)
                .setHeadersStrs(headers)
                .setParametersInfo(params)
                .setSettings(settings)
        ;
        
        return listener;
    }
    
    @SneakyThrows
    @Override
    public Object applyUnsafe(Object instance, Method method, Object[] args)
    {
        if (Modifier.isAbstract(method.getModifiers()))
        {
            return listeners.get(method).apply(instance, method, args);
        }
        
        return method.invoke(instance, args);
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }

}
