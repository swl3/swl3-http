package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface HttpInterface {
    public String host();
}
