package ru.swayfarer.swl3.network.http.proxy;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.reflect.Method;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.EnumHttpMethod;
import ru.swayfarer.swl3.network.http.client.HttpClient;
import ru.swayfarer.swl3.network.http.parts.HttpBody;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;
import ru.swayfarer.swl3.network.http.request.HttpRequestContent;
import ru.swayfarer.swl3.network.http.response.HttpResponse;

@Getter
@Setter
@Accessors(chain = true)
public class HttpMethodListener implements IProxyProvider.IMethodListener{

    @NonNull
    public HttpClient httpClient;
    
    @NonNull
    public String host;
    
    @NonNull
    public String uri;
    
    @NonNull
    public EnumHttpMethod basicMethod;
    
    @NonNull
    public ParametersInfo parametersInfo;
    
    @NonNull
    public ExtendedList<String[]> headersStrs = new ExtendedList<>();
    
    public IFunction0<ClientSettings> settings = () -> new ClientSettings();
    
    public HttpMethodListener() {}
    
    @Override
    public Object applyUnsafe(Object instance, Method method, Object[] args)
    {
        var response = httpClient.send(
                basicMethod, 
                getAddress(instance, method, args), 
                createHttpRequestContent(instance, method, args)
        );
        
        Object ret = null;
        
        var methodReturnType = method.getReturnType();
        
        if (HttpResponse.class.isAssignableFrom(methodReturnType))
        {
            ret = response;
        }
        else 
        {
            ret = response.getBody().getContent(method.getGenericReturnType());
        }
        
        return ret;
    }
    
    public ClientSettings getSettings()
    {
        return settings.apply();
    }

    public String getAddress(Object instance, Method method, Object[] args)
    {
        var host = this.host;
        var uri = this.uri;
        
        uri = replaceActors(uri, args);
        host = replaceActors(host, args);
        
        if (!host.endsWith("/") && !uri.startsWith("/"))
            host += "/";
        
        return host + uri;
    }
    
    public String replaceActors(String sourceStr, Object[] args) 
    {
        var settings = getSettings();
        
        for (var param : parametersInfo.parameters)
        {
            String name = param.getName();
            var actor = settings.getActorStart() + name + settings.getActorEnd();
            
            if (sourceStr.contains(actor))
            {
                sourceStr = sourceStr.replace(actor, args[param.getIndex()].toString());
            }
        }
        
        return sourceStr;
    }
    
    public HttpHeaders getHeaders(Object instance, Method method, Object[] args)
    {
        var headersStrs = this.headersStrs.copy();
        var headers = getHttpClient().newHeaders();
        
        for (var headerStr : headersStrs)
        {
            var str = replaceActors(headerStr[1], args);
            headers.value(headerStr[0], str);
        }
        
        return headers;
    }
    
    public HttpRequestContent createHttpRequestContent(Object instance, Method method, Object[] args)
    {
        HttpHeaders headers = getHeaders(instance, method, args);
        return httpClient
                .newRequestContent()
                .setHeaders(headers)
                .setBody(createBody(headers, instance, method, args))
        ;
    }
    
    public HttpBody createBody(HttpHeaders headers, Object instance, Method method, Object[] args)
    {
        var body = new HttpBody();
        body.setLazy(false);
        body.setHeaders(() -> headers);
        
        if (parametersInfo.hasBody())
        {
            body.setContent(args[parametersInfo.getBodyIndex()]);
        }
        
        return body;
    }
}
