package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import java.lang.reflect.Method;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;

public interface IProxyProvider {

    public <T> T createProxy(Class<?> proxyInterface, IMethodListener lisnenerFun);
    
    public interface IMethodListener extends IFunction3<Object, Method, Object[], Object> {}
    
}
