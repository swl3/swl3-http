package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParameterInfo {
    public int index;
    public String name;
    public EnumParameterType type = EnumParameterType.Simple;
}
