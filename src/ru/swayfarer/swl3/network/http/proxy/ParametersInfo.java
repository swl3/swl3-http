package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@Accessors(chain = true)
public class ParametersInfo {

    public int bodyIndex = -1;
    public int pathIndex = -1;
    
    @NonNull
    public ExtendedList<ParameterInfo> parameters = CollectionsSWL.list();
    
    public boolean hasBody()
    {
        return bodyIndex != -1;
    }
    
    public boolean hasPath()
    {
        return pathIndex != -1;
    }
    
}
