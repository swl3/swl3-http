package ru.swayfarer.swl3.network.http.proxy;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.swl3.network.http.EnumHttpMethod;

@Retention(RUNTIME)
public @interface RequestMethod {
    public EnumHttpMethod method() default EnumHttpMethod.Get;
    public String[] headers() default {};
    public String value() default "/";
}
