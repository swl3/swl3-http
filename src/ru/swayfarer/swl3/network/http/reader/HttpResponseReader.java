package ru.swayfarer.swl3.network.http.reader;

import lombok.var;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.network.http.parts.HttpStatus;
import ru.swayfarer.swl3.network.http.response.HttpResponse;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpResponseReader {
    
    public static int nextStatus = 0;
    
    public static int READING_TOP = nextStatus++;
    public static int READING_HEADERS = nextStatus++;
    public static int READING_BODY = nextStatus++;
    
    public IFunction0<HttpSettings> httpSettings = () -> new HttpSettings();
    public IFunction0<String> lineSplitter = () -> "\r\n";
    public String headerSplitter = ":";
    
    public HttpResponse readUTF8(@NonNull String responseStr)
    {
        return read(responseStr, StandardCharsets.UTF_8);
    }
    
    public HttpResponse read(@NonNull String responseStr, @NonNull Charset charset)
    {
        return read(BytesInStream.of(responseStr.getBytes(charset)), false);
    }
    
    public HttpResponse read(@NonNull DataInStream stream, boolean lazy)
    {
        var status = READING_TOP;
        
        var response = new HttpResponse()
                .setHttpSettings(httpSettings)
        ;
        
        String splitter = lineSplitter.apply();
        stream.getLinesReadingHelper().setLineSplitter(splitter);

        String ln = null;
        
        while ((ln = stream.readLine()) != null)
        {
            if (status != READING_BODY)
            {
                if (!StringUtils.isBlank(ln.replace("\r", "")))
                {
                    if (status == READING_TOP)
                    {
                        var split = ln.split(" ");
                        response.setProtocol(split[0]);
                        response.setStatus(new HttpStatus(Integer.valueOf(split[1])));
                        status = READING_HEADERS;
                    }
                    else if (status == READING_HEADERS)
                    {
                        var headers = response.getHeaders();
                        var readed = readHeader(ln);
                        
                        if (readed != null) 
                        {
                            headers.value(readed[0], readed[1]);
                        }
                    }
                }
                else
                {
                    if (status == READING_HEADERS)
                    {
                        status = READING_BODY;
                        readBody(stream, response);
                        break;
                    }
                }
            }
        }
        
        return response;
    }
    
    public String[] readHeader(String ln)
    {
        var indexOfSplitter = ln.indexOf(headerSplitter);
        var splitterLenght = headerSplitter.length();
        
        if (indexOfSplitter > 0 && indexOfSplitter < ln.length() - splitterLenght)
        {
            String key = ln.substring(0, indexOfSplitter);
            String headerValues = ln.substring(indexOfSplitter + splitterLenght).trim();
            
            for (var value : headerValues.split(";"))
            {
                return new String[] {key, value.trim()};
            }
        }
        
        return null;
    }
    
    public void readBody(@NonNull DataInStream stream, @NonNull HttpResponse response) 
    {
        var body = response.getBody();
        body.setInStream(stream);
    }
}
