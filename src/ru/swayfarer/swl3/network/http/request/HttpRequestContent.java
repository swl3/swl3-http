package ru.swayfarer.swl3.network.http.request;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.network.http.parts.HttpBody;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpRequestContent {
    
    @NonNull
    public IFunction0<HttpSettings> httpSettings;
    
    @NonNull
    public String protocol = "HTTP/1.1";
    
    @NonNull
    public HttpHeaders headers = new HttpHeaders()
        .setHttpSettings(() -> httpSettings.apply())
    ;
    
    @NonNull
    public HttpBody body = new HttpBody()
        .setHttpSettings(() -> httpSettings.apply())
        .setHeaders(this::getHeaders)
    ;
}
