package ru.swayfarer.swl3.network.http.request;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.network.http.EnumHttpMethod;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpRequestTarget {

    @NonNull
    public String uriMapping = "/";
    
    @NonNull
    public EnumHttpMethod method = EnumHttpMethod.Get;
    
}
