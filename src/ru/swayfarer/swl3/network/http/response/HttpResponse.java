package ru.swayfarer.swl3.network.http.response;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.HttpSettings;
import ru.swayfarer.swl3.network.http.parts.HttpBody;
import ru.swayfarer.swl3.network.http.parts.HttpHeaders;
import ru.swayfarer.swl3.network.http.parts.HttpStatus;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpResponse {

    @NonNull
    public String protocol = "HTTP/1.1";
    
    @NonNull
    public String message = "OK";
    
    @NonNull
    public HttpStatus status = new HttpStatus(200);
    
    @NonNull
    public IFunction0<HttpSettings> httpSettings = () -> new HttpSettings();
    
    @NonNull
    public HttpBody body = new HttpBody()
        .setHttpSettings(httpSettings::apply)
        .setHeaders(this::getHeaders)
    ;
    
    @NonNull
    public HttpHeaders headers = new HttpHeaders();
    
    public HttpResponse setHttpSettings(IFunction0<HttpSettings> httpSettings)
    {
        this.httpSettings = httpSettings;
        body.setHttpSettings(httpSettings);
        return this;
    }
}
