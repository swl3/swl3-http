package ru.swayfarer.swl3.network.http.utils;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

public class NetworkUtils {
    
    @Data
    @Accessors(chain = true)
    @AllArgsConstructor @NoArgsConstructor
    public static class HttpUrlInfo {
        public String uriMapping;
        public int port;
        public String host;
        public String protocol;
        public String login;
        public String password;
    }
    
}
