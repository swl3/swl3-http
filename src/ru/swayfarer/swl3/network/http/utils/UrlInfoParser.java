package ru.swayfarer.swl3.network.http.utils;

import lombok.var;
import java.util.Map;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.network.http.utils.NetworkUtils.HttpUrlInfo;

public class UrlInfoParser {

    public Map<String, Integer> defaultPorts = CollectionsSWL.map(
            "http", 80,
            "https", 443
    );
    
    public HttpUrlInfo parse(String addres)
    {
        int indexOfProtocol = addres.indexOf("://");
        int indexOfProtocolEnd = indexOfProtocol > 0 ? indexOfProtocol + 3 : 0;
        
        String protocol = "http";
        String host = addres;
        String uriMapping = "/";
        String login = null;
        String password = null;
        
        int port = -1;
        
        if (indexOfProtocol > 0)
        {
            host = host.substring(indexOfProtocolEnd);
        }
        
        int indexOfAuth = host.indexOf('@');
        
        if (indexOfAuth > 0)
        {
            String authStr = host.substring(0, indexOfAuth);
            host = host.substring(indexOfAuth + 1);
            
            int indexOfPassword = authStr.lastIndexOf(':');
            
            if (indexOfPassword > 0)
            {
                login = authStr.substring(0, indexOfPassword);
                password = authStr.substring(indexOfPassword + 1);
            }
            else
            {
                login = authStr;
            }
        }
        
        int indexOfPort = host.indexOf(':', indexOfProtocol);
        
        int indexOfHostEnd = host.indexOf("/", indexOfProtocolEnd);
        
        if (indexOfHostEnd <= 0)
        {
            indexOfHostEnd = host.length() - 1;
            uriMapping = "/";
        }
        else
            uriMapping = host.substring(indexOfHostEnd);
        
        if (indexOfPort > 0)
        {
            int indexOfPortStart = indexOfPort + 1;
            String portStr = host.substring(indexOfPortStart, indexOfHostEnd);
            
            port = Integer.valueOf(portStr);
            
            host = host.substring(0, indexOfPort);
        }
        else
        {
            host = host.substring(0, indexOfHostEnd);
        }

        if (port == -1)
        {
            var portObj = defaultPorts.get(protocol);
            
            if (portObj != null)
                port = portObj;
        }
        
        return new HttpUrlInfo()
                .setHost(host)
                .setPort(port)
                .setProtocol(protocol)
                .setUriMapping(uriMapping)
                .setLogin(login) 
                .setPassword(password)
        ;
    }
    
}
