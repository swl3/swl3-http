package ru.swayfarer.swl3.network.http.writer;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.network.http.request.HttpRequestContent;
import ru.swayfarer.swl3.network.http.request.HttpRequestTarget;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class HttpMessageWriter {

    @NonNull
    public IFunction0<String> lineSplitter = () -> "\n";
    
    public String write(@NonNull HttpRequestTarget target, @NonNull HttpRequestContent request)
    {
        var str = new DynamicString();
        str.setLineSplitter(lineSplitter.apply());
        
        str.append(target.getMethod().getMethodName());
        str.append(" ");
        str.append(target.getUriMapping());
        str.append(" ");
        str.append(request.getProtocol());
        str.newLine();
        
        boolean firstHeader = true;
        
        for (var header : request.getHeaders().getValues().entrySet())
        {
            if (!firstHeader)
                str.newLine();

            firstHeader = false;
            str.append(header.getKey());
            str.append(": ");
            str.append(StringUtils.concatWith(";", header.getValue().toArray()));
        }
        
        str.newLine();
        
        str.append(request.getBody().getContentStr());
        
        str.newLine(3);
        
        return str.toString();
    }
}
